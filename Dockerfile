# Create build stage based on buster image
FROM golang:1.18-buster AS builder
# Create working directory under /app
WORKDIR /app
# Copy over all go config (go.mod, go.sum etc.)
COPY go.mod go.sum ./
RUN go mod download
# Copy app files
COPY . .
# Run the Go build and output binary under hello_go_http
RUN go build -o /api_server
# Make sure to expose the port the HTTP server is using
EXPOSE 8080
# Run the app binary when we run the container
ENTRYPOINT ["/api_server"]