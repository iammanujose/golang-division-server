**RUN**

```
git clone https://gitlab.com/iammanujose/golang-division-server.git
cd golang-division-server
docker build -t api_server .
docker run -p 8080:8080 api_server
```

Check POST [http://localhost:8080/v1/divide/10/5](http://localhost:8080/v1/divide/10/5)

Curl
```
curl --request POST \
  --url http://localhost:8080/v1/divide/10/5
```

**Test**
To run Unit test
```
go test ./...
```