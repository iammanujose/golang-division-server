package division

import "errors"

type result struct {
	a float64
	b float64
}

// Divide used to divide two num
func Divide(a, b float64) (result, error) {
	if a == 0 || b == 0 {
		return result{}, errors.New("division by zero")
	}
	return result{a / b, b / a}, nil
}
