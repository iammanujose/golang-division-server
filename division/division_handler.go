package division

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func DivideHandler(c *gin.Context) {
	firstParamStr := c.Param("a")
	secondParamStr := c.Param("b")

	firstParamNum, err := strconv.ParseFloat(firstParamStr, 64)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, DivisionResult{Msg: http.StatusText(http.StatusBadRequest), Errors: []string{"invalid number"}})
		return
	}
	secondParamNum, err := strconv.ParseFloat(secondParamStr, 64)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, DivisionResult{Msg: http.StatusText(http.StatusBadRequest), Errors: []string{"invalid number"}})
		return
	}

	if firstParamNum == 0 || secondParamNum == 0 {
		log.Println(fmt.Errorf("divide by zero"))
		c.JSON(http.StatusBadRequest, DivisionResult{Msg: http.StatusText(http.StatusBadRequest), Errors: []string{"division by zero"}})
		return
	}

	response, err := Divide(firstParamNum, secondParamNum)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, DivisionResult{Msg: http.StatusText(http.StatusBadRequest), Errors: []string{err.Error()}})
		return
	}
	c.JSON(http.StatusOK, DivisionResult{&response.a, &response.b, http.StatusText(http.StatusOK), []string{}})
}
