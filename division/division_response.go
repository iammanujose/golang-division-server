package division

type DivisionResult struct {
	A      *float64 `json:"a/b,omitempty"`
	B      *float64 `json:"b/a,omitempty"`
	Msg    string   `json:"message"`
	Errors []string `json:"errors"`
}
