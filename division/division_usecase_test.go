package division

import (
	"testing"
)

type divideTest struct {
	a        float64
	b        float64
	expected result
	err      error
}

func TestDivide(t *testing.T) {
	var tests = []divideTest{
		divideTest{2, 3, result{0.6666666666666666, 1.500000}, nil},
		divideTest{4, 8, result{0.5, 2.0}, nil},
		divideTest{6, 9, result{0.6666666666666666, 1.5}, nil},
		divideTest{3, 10, result{0.3, 3.3333333333333335}, nil},
	}

	got, _ := Divide(4, 2)
	a, b := 2.0, 0.5

	if a != got.a || b != got.b {
		t.Errorf("got %f,%f, wanted %f,%f", got.a, got.b, a, b)
	}
	for _, test := range tests {
		if output, _ := Divide(test.a, test.b); output.a != test.expected.a || output.b != test.expected.b {
			t.Errorf("got %v,%v, wanted %v,%v", output.a, output.b, test.expected.a, test.expected.b)
		}
	}
}
