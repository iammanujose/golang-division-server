package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/iammanujose/api/router"
)

func main() {

	r := gin.Default()

	//initialize router
	router.InitializeRouter(r)

	//starting api server
	r.Run(":8080")
}
