package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/iammanujose/api/division"
)

func InitializeRouter(router *gin.Engine) {
	// Simple group: v1
	v1 := router.Group("/v1")
	{
		v1.POST("/divide/:a/:b", division.DivideHandler)
	}
}
